class ApplicationController < ActionController::API
  respond_to :json

  def sequence
    render 'layouts/application'
  end

  def create_list
    list = JobsList.new(params[:seq]).create_list
    render :json => { jobs_list: list }
  end
end
