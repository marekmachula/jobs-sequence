class JobsList
  WITH_DEPENDENCY = /[a-z](=>)[a-z]/

  def initialize(sequence)
    @sequence = sequence
  end

  def create_list
    return "''" if @sequence.nil?
    job_signs = @sequence.map {|s| s[:sign]}
    analyze(job_signs)
  end

  def analyze(signs)
    all_signs_count = signs.join.gsub!('=>','').split('').uniq.count
    without_dependency = signs.select{|sign| !(WITH_DEPENDENCY =~ sign) }.map {|sign| sign.gsub!('=>','')}
    with_dep = signs.select{|sign| WITH_DEPENDENCY =~ sign }.map {|sign| sign.gsub!('=>','').reverse }
    without_dependency.delete_if {|sign| with_dep.join.include? sign }
    return 'Jobs can’t depend	on themselves.!' if with_dep && with_dep.select {|sign| sign.first == sign.last}.any?

    (with_dep.size - 2).times do
      with_dep.sort! do |a,b|
        [a.last, b.first] <=> [b.first, a.last]
      end
    end
    with_dep.sort! do |a,b|
      ([a.last, b.first] <=> [b.first, a.last]) == 0 ? 0 : 1
    end

    list = with_dep.join.squeeze << without_dependency.join
    return 'Unacceptable circle dependency !!!!' if list.size > all_signs_count
    list
  end
end