(function (){
    angular.module('jobsSequence', [])
        .controller('MainCtrl', [
        '$scope',
        '$http',
        function($scope, $http){
            $scope.result = '';
            $scope.jobs = [];

            $scope.addJob = function(){
                if(!$scope.job_sign || $scope.job_sign === '') { return; }
                $scope.jobs.push({sign: $scope.job_sign});
                $scope.job_sign = '';
            };

            $scope.createList = function(){
              if(!$scope.jobs || $scope.jobs === []) { return; }
              $http.post('/create_list.json', { seq: $scope.jobs}).success(function(data){
                $scope.result = data.jobs_list;
              });
            };
        }]);
})();