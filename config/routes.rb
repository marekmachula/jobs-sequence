Rails.application.routes.draw do
  root 'application#sequence'

  post 'create_list' => 'application#create_list'
end
