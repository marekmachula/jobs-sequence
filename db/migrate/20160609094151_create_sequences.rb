class CreateSequences < ActiveRecord::Migration
  def change
    create_table :sequences do |t|
      t.string :jobs_structure

      t.timestamps null: false
    end
  end
end
